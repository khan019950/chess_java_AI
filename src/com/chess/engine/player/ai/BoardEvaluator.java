package com.chess.engine.player.ai;

import com.chess.engine.board.Board;

public interface BoardEvaluator {

    int evaluate(Board board, int depth);//+ve = white winning || -ve = black winnig ||0 = tie
}
